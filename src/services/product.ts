import http from "@/services/http"
import type { Product } from "@/types/Product"

function addProduct(product: Product) {
    return http.post('/products', product)
}

function updateProduct(product: Product) {
    return http.patch(`/products/${product.id}`, product)
}
  
function delProduct(product: Product) {
    return http.delete(`/products/${product.id}`)
}

function getProduct(id: number) {
    return http.get(`/products/${id}`)
}

function getProducts() {
    return http.get('/products')
}


export default { addProduct, updateProduct, delProduct, getProduct, getProducts }
