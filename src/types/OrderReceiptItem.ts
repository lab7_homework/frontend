type OrderReceiptItem = {
    id: number
    name: string
    price: number
    qty: number
}

export type { OrderReceiptItem }