type Category = 'Drink' | 'Bakery ' | 'Others'
type Product = {
    id: number
    name: string
    price: number
    category: Category
}
export type { Product, Category }