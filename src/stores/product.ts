import { ref } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product'
import { useLoadingStore } from './loading'
import type { Product } from '@/types/Product'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const products = ref<Product[]>([])
  
  async function getProduct(id: number) {
    loadingStore.doload()
    const res = await productService.getProduct(id)
    products.value = res.data
    loadingStore.finish()
  }

  async function getProducts() {
    loadingStore.doload()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }
  
  async function saveProduct(product: Product) {
    loadingStore.doload()
    if (product.id < 0) {
      console.log('Post ' + JSON.stringify(product))
      const res = await productService.addProduct(product)
    } else {
      console.log('Patch ' + JSON.stringify(product))
      const res = await productService.updateProduct(product)
    }
    await getProducts()
    loadingStore.finish()
  }
  
  async function deleteProduct(product: Product) {
    loadingStore.doload()
    const res = await productService.delProduct(product)
    await getProducts()
    loadingStore.finish()
  }
  return { products, getProducts, saveProduct, deleteProduct, getProduct}
})
