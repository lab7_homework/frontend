import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currantUser = ref<User>({
    id: 1,
    email: 'mana123@gmail.com',
    password: 'pass123',
    fullname: 'Poomrapee Sadsamai',
    gender: 'male',
    roles: ['manager']
  })
  return { currantUser }
})
